﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade.DTO;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Repositorio;
using Treinamento.Net.Repositorio.Conexao;

namespace Treinamento.Net.Repositorio.Repositorio
{
    public class PedidoRepositorio : IPedidoRepositorio
    {
        private readonly IConexaoSqlServer _conexaoSql;
        public PedidoRepositorio(IConexaoSqlServer conexaoSqlServer)
        {
            _conexaoSql = conexaoSqlServer;
        }

        public bool alterarStatus(int id, int status)
        {
            string sqlConsulta = @"update pedido set
                                           in_status = @status
                                            where id = @id";

            return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, new { id, status }) > 0;
        }

        public Pedido consultarPedido(int id)
        {
            Pedido pedido = new Pedido();

            string sqlConsulta = @"select   id_cliente as codigoCliente,
                                            cd_cep_cliente as cep,
                                            in_status as statusPedido
                                    from pedido where id = @id";

            pedido = Dapper.SqlMapper.QueryFirstOrDefault<Pedido>(_conexaoSql.AbrirConexao(), sqlConsulta, new { id });

            string sqlItem = @"select   id_pedido as NumeroPedido,
                                        id_produto as codigoProduto,
                                        nu_quantidade as quantidadeProduto,
                                        vl_preco as valorProduto
                                    from pedido_item where id_pedido = @id";

            pedido.items = Dapper.SqlMapper.Query<PedidoItem>(_conexaoSql.AbrirConexao(), sqlItem, new { id }).ToList();

            return pedido;
        }

        public int incluir(Pedido pedido)
        {
            string sql = @"insert into pedido (id_cliente,cd_cep_cliente,in_status) 
                           OUTPUT INSERTED.id
                                            values (@codigoCliente,@cep,@statusPedido)";
            string sqlItem = @"insert into pedido_item (id_pedido,id_produto,nu_quantidade,vl_preco) 
                                            values (@NumeroPedido,@codigoProduto,@quantidadeProduto,@valorProduto)";

            var transacao = _conexaoSql.Transaction();

            try
            {
                int novoid = Dapper.SqlMapper.QuerySingle<int>(_conexaoSql.AbrirConexao(), sql, pedido);
                //int novoid = Dapper.SqlMapper.QuerySingle<int>(_conexaoSql.AbrirConexao(), sql, pedido,transacao);
                pedido.items.ForEach(item => item.numeroPedido = novoid);
                int linhasafetadas = Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlItem, pedido.items);
                //int linhasafetadas = Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlItem, pedido.items,transacao);
                transacao.Commit();
                return novoid;
            }
            catch (Exception ex)
            {

                transacao.Rollback();
            }
            finally
            {
                transacao.Dispose();
            }
            return 0;
        }

        public List<Pedido> listar()
        {
            List<Pedido> pedidosTemp = new List<Pedido>();
            List<Pedido> pedidos = new List<Pedido>();

            string sqlConsulta = @"select   id as id,
                                            id_cliente as codigoCliente,
                                            cd_cep_cliente as cep,
                                            in_status as statusPedido
                                    from pedido";

            pedidosTemp = Dapper.SqlMapper.Query<Pedido>(_conexaoSql.AbrirConexao(), sqlConsulta).ToList();

            string sqlItem = @"select   id_pedido as NumeroPedido,
                                        id_produto as codigoProduto,
                                        nu_quantidade as quantidadeProduto,
                                        vl_preco as valorProduto
                                    from pedido_item where id_pedido = @id";

            foreach(var pedido in pedidosTemp)
            {
                var pedidoItem = Dapper.SqlMapper.Query<PedidoItem>(_conexaoSql.AbrirConexao(), sqlItem, new { pedido.id }).ToList();
                pedido.items = pedidoItem;
                pedidos.Add(pedido);
            }

            return pedidos;
        }
    }
}
