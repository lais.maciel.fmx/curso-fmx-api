﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Repositorio;
using Treinamento.Net.Repositorio.Conexao;

namespace Treinamento.Net.Repositorio.Repositorio
{
    public class ClienteRepositorio : IClienteRepositorio
    {
        private readonly IConexaoSqlServer _conexaoSql;
        public ClienteRepositorio(IConexaoSqlServer conexaoSqlServer)
        {
            _conexaoSql = conexaoSqlServer;
        }

        public ClienteResponse Consultar(int codigoCliente)
        {
            string sqlConsulta = @"SELECT id as codigoCliente,nm_cliente as nomeCliente,cd_cpf as cpfCliente FROM cliente where id = @codigoCliente";

            return Dapper.SqlMapper.QueryFirstOrDefault<ClienteResponse>(_conexaoSql.AbrirConexao(), sqlConsulta, new { codigoCliente });
        }

        public ClienteResponse ConsultarCPF(string cpf)
        {
            string sqlConsulta = @"SELECT id as codigoCliente,nm_cliente as nomeCliente,cd_cpf as cpfCliente FROM cliente where cd_cpf = @cpf";

            return Dapper.SqlMapper.QueryFirstOrDefault<ClienteResponse>(_conexaoSql.AbrirConexao(), sqlConsulta, new { cpf });
        }

        public bool Incluir(ClienteResquest cliente)
        {
            string sqlConsulta = @"insert into cliente (nm_cliente,cd_cpf) values (@nomeCliente,@cpfCliente)";
            try
            {
                return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, cliente) > 0;
            }
            catch (Exception)
            {

                return false;
            }
            

        }

        public bool Alterar(int codigoCliente,string nome)
        {
            string sqlConsulta = @"update cliente set
                                           nm_cliente = @nomeCliente
                                            where id = @codigo";

            return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta,new { codigo = codigoCliente, nomeCliente = nome }) > 0;
        }

        public List<ClienteResponse> Listar()
        {
            string sqlConsulta = @"SELECT id as codigoCliente,nm_cliente as nomeCliente,cd_cpf as cpfCliente FROM cliente";
            return Dapper.SqlMapper.Query<ClienteResponse>(_conexaoSql.AbrirConexao(), sqlConsulta).ToList();
        }

        public bool Eliminar(int id)
        {
            string sqlConsulta = @"delete from cliente where id = @codigo";
            try
            {
                return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, new { codigo = id }) > 0;
            }
            catch (Exception)
            {

                return false;
            }
            
        }
    }
}
