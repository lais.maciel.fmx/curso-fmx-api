﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Repositorio;
using Treinamento.Net.Repositorio.Conexao;

namespace Treinamento.Net.Repositorio.Repositorio
{
    public class ProdutoRepositorio : IProdutoRepositorio
    {
        private readonly IConexaoSqlServer _conexaoSql;
        public ProdutoRepositorio(IConexaoSqlServer conexaoSqlServer)
        {
            _conexaoSql = conexaoSqlServer;
        }

        public bool Alterar(int id, string descricao, string ean)
        {
            string sqlConsulta = @"update produto set
                                           cd_ean = @ean,
                                           ds_produto = @descricao
                                            where id = @id";

            return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, new { id,descricao, ean }) > 0;
        }

        public object Consultar(int id)
        {
            string sqlConsulta = @"SELECT id as codigoProduto, cd_ean as codigoEan, ds_produto as descricaoProduto FROM produto where id = @id";

            return Dapper.SqlMapper.QueryFirstOrDefault<ProdutoResponse>(_conexaoSql.AbrirConexao(), sqlConsulta, new { id });
        }

        public List<ProdutoResponse> ConsultarEAN(IEnumerable<string> ean)
        {
            try
            {
                string sqlConsulta = $@"SELECT  id as codigoProduto, 
                                                cd_ean as codigoEan, 
                                                ds_produto as descricaoProduto 
                                        FROM produto where cd_ean in('{ string.Join("', '",ean) }')";
                var retorno =  Dapper.SqlMapper.Query<ProdutoResponse>(_conexaoSql.AbrirConexao(), sqlConsulta).ToList();
                return retorno;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

        public bool Eliminar(int id)
        {
            string sqlConsulta = @"delete from produto where id = @id";
            try
            {
                return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, new { id }) > 0;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Incluir(ProdutoRequest produto)
        {
            string sqlConsulta = @"insert into produto (cd_ean,ds_produto) values (@codigoEan,@descricaoProduto)";
            try
            {
                return Dapper.SqlMapper.Execute(_conexaoSql.AbrirConexao(), sqlConsulta, produto) > 0;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<ProdutoResponse> listar()
        {
            string sqlConsulta = @"SELECT id as codigoProduto,cd_ean as codigoEan,ds_produto as descricaoProduto FROM produto";
            return Dapper.SqlMapper.Query<ProdutoResponse>(_conexaoSql.AbrirConexao(), sqlConsulta).ToList();
        }
    }
}
