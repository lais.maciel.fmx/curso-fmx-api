﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Repositorio.Conexao
{
   public class ConexaoSqlServer : IConexaoSqlServer
    {
        private IDbConnection _db;
        private string _connectionString;

        public ConexaoSqlServer(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("CursoApiConnection");
        }

        public IDbConnection AbrirConexao()
        {
            _db = new System.Data.SqlClient.SqlConnection(_connectionString);
            try
            {
                _db.Open();
            }
            catch (Exception )
            {
                //TODO: adicionar log
                throw;
            }
            return _db;
        }

        public IDbTransaction Transaction()
        {
            if (_db?.State != ConnectionState.Open)
                AbrirConexao();
            return _db.BeginTransaction();
        }

        public void FecharConexao()
        {
            if(_db.State == ConnectionState.Open)
            {
                _db.Close();
            }
            _db.Dispose();
            _db = null;
        }
    }
}
