﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Repositorio.Conexao
{
    public interface IConexaoSqlServer
    {
        IDbConnection AbrirConexao();
        IDbTransaction Transaction();
        void FecharConexao();
    }
}
