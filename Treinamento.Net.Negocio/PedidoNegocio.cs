﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.DTO;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;
using Treinamento.Net.Dominio.Interface.Repositorio;

namespace Treinamento.Net.Negocio
{
    public class PedidoNegocio : IPedidoNegocio
    {
        private readonly IPedidoRepositorio _pedidoRepositorio;
        private readonly IClienteNegocio _clienteNegocio;
        private readonly IProdutoNegocio _produtoNegocio;

        public PedidoNegocio(IPedidoRepositorio pedidoRepositorio, 
            IProdutoNegocio produtoNegocio, IClienteNegocio clienteNegocio)
        {
            _pedidoRepositorio = pedidoRepositorio;
            _clienteNegocio = clienteNegocio;
            _produtoNegocio = produtoNegocio;
        }

        public bool alterarStatus(int id, int status)
        {
            return _pedidoRepositorio.alterarStatus(id, status);
        }

        public Pedido consultarPedido(int id)
        {
            return _pedidoRepositorio.consultarPedido(id);
        }

        public BaseResponse incluir(PedidoRequest pedidoRequest)
        {
            var numeroPedido = 0;
            try
            {
                var cliente = _clienteNegocio.ConsultarCPF(pedidoRequest.cpfCliente);
                if (cliente != null)
                {
                    Pedido pedido = new Pedido();
                    pedido.codigoCliente = cliente.codigoCliente;
                    pedido.cep = pedidoRequest.cepCliente;
                    List<ProdutoResponse> listProduto = _produtoNegocio.ConsultarEAN(pedidoRequest.itensProduto.Select(item => item.eanProduto));
                    if(listProduto != null)
                    {
                        foreach (var item in pedidoRequest.itensProduto)
                        {
                            var produto = listProduto.FirstOrDefault(p => p.codigoEan == item.eanProduto);
                            if (!string.IsNullOrEmpty(produto.codigoEan) && item.quantidadeProduto > 0)
                            {
                                pedido.items.Add(new PedidoItem
                                {
                                    codigoProduto = produto.codigoProduto,
                                    valorProduto = item.valorProduto,
                                    quantidadeProduto = item.quantidadeProduto
                                });
                            }
                        }
                        if (pedido.items.Count > 0)
                        {
                            numeroPedido = _pedidoRepositorio.incluir(pedido);

                            if(numeroPedido != 0)
                                return new BaseResponse
                                {
                                    sucesso = true,
                                    mensagem = "Pedido Realizado com Sucesso.",
                                    data =  new { NumeroPedido = numeroPedido }
                                };
                        }
                        else
                        {
                            return new BaseResponse
                            {
                                sucesso = false,
                                mensagem = "Produto não localizado."
                            };
                        }
                        
                    }
                    else
                    {
                        return new BaseResponse
                        {
                            sucesso = false,
                            mensagem = "Produto não localizado."
                        };
                    } 
                }
                else
                {
                    return new BaseResponse
                    {
                        sucesso = false,
                        mensagem = "Informe um cliente válido."
                    };
                }
            }
            catch (Exception)
            {
                return new BaseResponse
                {
                    sucesso = false,
                    mensagem = "Erro ao cadastrar pedido."
                };
            }
            return new BaseResponse
            {
                sucesso = false,
                mensagem = "Erro ao cadastrar pedido."
            };

        }

        public List<Pedido> listar()
        {
            return _pedidoRepositorio.listar();
        }
    }
}
