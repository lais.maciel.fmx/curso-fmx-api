﻿using System.Collections.Generic;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;
using Treinamento.Net.Dominio.Interface.Repositorio;

namespace Treinamento.Net.Negocio
{
    public class ProdutoNegocio : IProdutoNegocio
    {
        private readonly IProdutoRepositorio _produtoRepositorio;
        public ProdutoNegocio(IProdutoRepositorio produtoRepositorio)
        {
            _produtoRepositorio = produtoRepositorio;
        }

        public bool Alterar(int id, string descricao, string ean)
        {
            return _produtoRepositorio.Alterar(id, descricao, ean);
        }

        public object Consultar(int id)
        {
            return _produtoRepositorio.Consultar(id);
        }

        public List<ProdutoResponse> ConsultarEAN(IEnumerable<string> enumerable)
        {
            return _produtoRepositorio.ConsultarEAN(enumerable);
        }

        public bool Eliminar(int id)
        {
            return _produtoRepositorio.Eliminar(id);
        }

        public bool Incluir(ProdutoRequest produto)
        {
            return _produtoRepositorio.Incluir(produto);
        }

        public List<ProdutoResponse> listar()
        {
            return _produtoRepositorio.listar();
        }
    }
}
