﻿using System.Collections.Generic;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;
using Treinamento.Net.Dominio.Interface.Repositorio;

namespace Treinamento.Net.Negocio
{
    public class ClienteNegocio : IClienteNegocio
    {
        private readonly IClienteRepositorio _clienteRepositorio;
        public ClienteNegocio(IClienteRepositorio clienteRepositorio)
        {
            _clienteRepositorio = clienteRepositorio;
        }

        public bool Alterar(int codigoCliente, string nome)
        {
            if(codigoCliente > 0)
            {
                return _clienteRepositorio.Alterar(codigoCliente, nome);
            }
            return false;
        }

        public ClienteResponse Consultar(int codigoCliente)
        {
            if (codigoCliente > 0)
            {
                return _clienteRepositorio.Consultar(codigoCliente);
            }
            return null;
        }

        public ClienteResponse ConsultarCPF(string cpf)
        {
            if (!string.IsNullOrEmpty(cpf))
            {
                return _clienteRepositorio.ConsultarCPF(cpf);
            }
            return null;
        }

        public bool Eliminar(int id)
        {
            return _clienteRepositorio.Eliminar(id);
        }

        public bool Incluir(ClienteResquest cliente)
        {
            return _clienteRepositorio.Incluir(cliente);
        }

        public List<ClienteResponse> Listar()
        {
            return _clienteRepositorio.Listar();
        }
    }
}
