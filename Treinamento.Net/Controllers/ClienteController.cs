﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Treinamento.Net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteNegocio _clienteNegocio;
        public ClienteController(IClienteNegocio clienteNegocio)
        {
            _clienteNegocio = clienteNegocio;
        }

        // GET: api/<ClienteController>
        /// <summary>
        /// Consultar Todos os Clientes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_clienteNegocio.Listar());
        }

        // GET api/<ClienteController>/5
        /// <summary>
        /// Consultar por ID
        /// </summary>
        /// <param name="id">Código do Cliente</param>
        /// <returns>Retorna o cliente</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var cliente = _clienteNegocio.Consultar(id);

            if (cliente == null)
                return NotFound(new
                {
                    sucesso = false,
                    mensagem = "Cliente Não Encontrado."
                });

            return Ok(cliente);
        }

        // POST api/<ClienteController>
        /// <summary>
        /// Incluir Cliente
        /// </summary>
        /// <param name="cliente">Cliente</param>
        [HttpPost]
        public IActionResult Post([FromBody] ClienteResquest cliente)
        {
            bool clientecadastrado = _clienteNegocio.Incluir(cliente);
            if (clientecadastrado)
            {
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Cliente Incluído com Sucesso."
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Erro ao Incluir Cliente."
            });
        }

        // PUT api/<ClienteController>/5
        /// <summary>
        /// Atualizar Cliente
        /// </summary>
        /// <param name="id">Código do Cliente</param>
        /// <param name="nomeCliente">Nome do Cliente</param>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] string nomeCliente)
        {
            bool clienteAlterado = _clienteNegocio.Alterar(id, nomeCliente);
            if (clienteAlterado)
            {
                var cliente = _clienteNegocio.Consultar(id);
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Cliente Alterado.",
                    data = cliente
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Cliente Não Encontrado."
            });
        }

        // DELETE api/<ClienteController>/5
        /// <summary>
        /// Eliminar Cliente
        /// </summary>
        /// <param name="id">Código do Cliente</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool clienteExcluido = _clienteNegocio.Eliminar(id);
            if (clienteExcluido)
            {
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Cliente Excluído com Sucesso."
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Erro ao Excluir Cliente."
            });
        }
    }
}
