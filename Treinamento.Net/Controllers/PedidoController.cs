﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Treinamento.Net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        public readonly IPedidoNegocio _PedidoNegocio;
        public PedidoController(IPedidoNegocio pedidoNegocio)
        {
            _PedidoNegocio = pedidoNegocio;
        }

        // POST api/<PedidoController>
        /// <summary>
        /// Incluir Pedido
        /// </summary>
        /// <param name="pedidoRequest">pedido</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] PedidoRequest pedidoRequest)
        {
            if (string.IsNullOrEmpty(pedidoRequest.cpfCliente))
            {
                return StatusCode(412, new
                {
                    sucesso = false,
                    mensagem = "Informe o CPF."
                });
            }
            if (string.IsNullOrEmpty(pedidoRequest.cepCliente))
            {
                return StatusCode(412, new
                {
                    sucesso = false,
                    mensagem = "Informe o CEP."
                });
            }
            if (pedidoRequest.itensProduto.Count <= 0)
            {
                return StatusCode(412, new
                {
                    sucesso = false,
                    mensagem = "Quantidade de Produtos inválida."
                });
            }

            BaseResponse response = _PedidoNegocio.incluir(pedidoRequest);
            if (response.sucesso)
                return Ok(response);

            return NotFound(response);

        }

        /// <summary>
        /// Alterar situação do pedido
        /// </summary>
        /// <param name="id">código do pedido</param>
        /// <param name="status">status</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, int status)
        {
            var pedidoAlterado = _PedidoNegocio.alterarStatus(id, status);
            if (pedidoAlterado)
                return Ok(new BaseResponse
                {
                    sucesso = true,
                    mensagem = "Status Alterado com Sucesso."
                });
            return NotFound(new BaseResponse
            {
                sucesso = false,
                mensagem = "Não foi possivel alterar situação."
            });
        }

        /// <summary>
        /// Consultar Pedido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var pedido = _PedidoNegocio.consultarPedido(id);
            if (pedido != null)
                return Ok(new BaseResponse
                {
                    sucesso = true,
                    mensagem = "Consulta realizada com sucesso.",
                    data = pedido
                });
            return NotFound(new BaseResponse
            {
                sucesso = false,
                mensagem = "Não foi possivel localizar pedido."
            });
        }

        /// <summary>
        /// Listar todos os pedidos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_PedidoNegocio.listar());
        }
    }
}
