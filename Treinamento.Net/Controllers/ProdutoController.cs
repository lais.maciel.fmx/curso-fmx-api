﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade.Parametros;
using Treinamento.Net.Dominio.Interface.Negocio;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Treinamento.Net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        public readonly IProdutoNegocio _produtoNegocio;
        public ProdutoController(IProdutoNegocio produtoNegocio)
        {
            _produtoNegocio = produtoNegocio;
        }

        // GET: api/<ProdutoController>
        /// <summary>
        /// Listar todos os produtos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_produtoNegocio.listar());
        }

        // GET api/<ProdutoController>/5
        /// <summary>
        /// Consultar por código
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var produto = _produtoNegocio.Consultar(id);

            if (produto == null)
                return NotFound(new
                {
                    sucesso = false,
                    mensagem = "Produto Não Encontrado."
                });

            return Ok(produto);
        }


        // POST api/<ProdutoController>
        /// <summary>
        /// Incluir produto
        /// </summary>
        /// <param name="produto"> produto</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] ProdutoRequest produto)
        {
            bool produtoCadastrado = _produtoNegocio.Incluir(produto);
            if (produtoCadastrado)
            {
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Produto Incluído com Sucesso."
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Erro ao Incluir Produto."
            });
        }

        // PUT api/<ProdutoController>/5
        /// <summary>
        /// Alterar produto
        /// </summary>
        /// <param name="id">código do produto</param>
        /// <param name="ean">código ean</param>
        /// <param name="descricao">descrição</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, string ean, string descricao)
        {
            if (descricao == null) {
                return NotFound(new
                {
                    sucesso = false,
                    mensagem = "Informe Descrição."
                });
            }
            if (ean == null) {
                return NotFound(new
                {
                    sucesso = false,
                    mensagem = "Informe Código do EAN."
                });
            }
            
            bool produtoAlterado = _produtoNegocio.Alterar(id, descricao, ean);
            if (produtoAlterado)
            {
                var cliente = _produtoNegocio.Consultar(id);
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Produto Alterado.",
                    data = cliente
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Produto Não Encontrado."
            });            
        }

        // DELETE api/<ProdutoController>/5
        /// <summary>
        /// Eliminar Cliente
        /// </summary>
        /// <param name="id">código do cliente</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            bool produtoExcluido = _produtoNegocio.Eliminar(id);
            if (produtoExcluido)
            {
                return Ok(new
                {
                    sucesso = true,
                    mensagem = "Produto Excluído com Sucesso."
                });
            }
            return NotFound(new
            {
                sucesso = false,
                mensagem = "Erro ao Excluir Produto."
            });
        }
    }
}
