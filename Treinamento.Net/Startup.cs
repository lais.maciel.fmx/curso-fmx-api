using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using Treinamento.Net.Dominio.Interface.Negocio;
using Treinamento.Net.Dominio.Interface.Repositorio;
using Treinamento.Net.Negocio;
using Treinamento.Net.Repositorio.Conexao;
using Treinamento.Net.Repositorio.Repositorio;
using Treinamento.Net.Repositorio.Middleware;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System;

namespace Treinamento.Net
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            DependecyInjection(services);
            services.AddSwaggerGen();
           
            carregaSwagger(services);
            
            services.AddRouting(options => options.LowercaseUrls = true);

            //Menor trafego de dados
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal);
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });

            // O m�todo AddJsonOptions permite a customiza��o das configura��es de serializa��o
            // Ignorar propriedades nulas
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });

            services.AddControllers();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<MiddlwareExcecaoGlobal>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Curso");
                c.RoutePrefix = "api/swagger";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.Use(async (context, next) => {

                string ApiKeyName = "ApiKey";

                if (!context.Request.Headers.TryGetValue(ApiKeyName, out var keyRecebida))
                {
                    context.Response.StatusCode = 403;

                    context.Response.StatusCode = 403;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                    {
                        sucesso = false,
                        mensagem = "Acesso n�o autorizado",
                        dataHora = DateTime.Now
                    }));

                    return;
                }

                var configuration = context.RequestServices.GetRequiredService<IConfiguration>();
                var apiKey = configuration.GetValue<string>("ApiKey");

                if (!apiKey.Equals(keyRecebida))
                {
                    context.Response.StatusCode = 403;
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                    {
                        sucesso = false,
                        mensagem = "Acesso n�o autorizado",
                        dataHora = DateTime.Now
                    }));

                    return;
                }

                await next();

            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void DependecyInjection(IServiceCollection services)
        {
            #region Negocio
            services.AddScoped<IClienteNegocio, ClienteNegocio>();
            services.AddScoped<IPedidoNegocio,PedidoNegocio>();
            services.AddScoped<IProdutoNegocio, ProdutoNegocio>();
            #endregion Negocio

            #region Infraestrutura
            services.AddScoped<IConexaoSqlServer, ConexaoSqlServer>();
            services.AddScoped<IClienteRepositorio, ClienteRepositorio>();
            services.AddScoped<IProdutoRepositorio, ProdutoRepositorio>();
            services.AddScoped<IPedidoRepositorio, PedidoRepositorio>();
            #endregion Infraestrutura
        }

        private void carregaSwagger(IServiceCollection services)
        {
            // Configurando o servi�o de documenta��o do Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Api Core - Curso",
                    Version = Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    Description = "Api Curso",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "Curso",
                        Email = string.Empty,
                    },
                    License = new Microsoft.OpenApi.Models.OpenApiLicense
                    {
                        Name = "� Copyright Curso.Todos os Direitos Reservados.",
                    }
                });


                string caminhoAplicacao = PlatformServices.Default.Application.ApplicationBasePath;
                string nomeAplicacao = PlatformServices.Default.Application.ApplicationName;
                string caminhoXmlDoc = Path.Combine(caminhoAplicacao, $"{nomeAplicacao}.xml");

                c.IncludeXmlComments(caminhoXmlDoc);

            });
        }
    }
}
