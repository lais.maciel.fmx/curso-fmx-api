﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;

namespace Treinamento.Net.Dominio.Interface.Negocio
{
    public interface IProdutoNegocio
    {
        List<ProdutoResponse> listar();
        object Consultar(int id);
        bool Incluir(ProdutoRequest produto);
        bool Alterar(int id, string descricao, string ean);
        bool Eliminar(int id);
        List<ProdutoResponse> ConsultarEAN(IEnumerable<string> enumerable);
    }
}
