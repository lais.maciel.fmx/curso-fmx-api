﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.Parametros;

namespace Treinamento.Net.Dominio.Interface.Negocio
{
    public interface IClienteNegocio
    {
        ClienteResponse Consultar(int codigoCliente);
        public List<ClienteResponse> Listar();
        Boolean Incluir(ClienteResquest cliente);
        bool Alterar(int codigoCliente, string nome);
        bool Eliminar(int id);
        ClienteResponse ConsultarCPF(string cpf);
    }
}
