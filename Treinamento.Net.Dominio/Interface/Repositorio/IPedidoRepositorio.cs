﻿using System.Collections.Generic;
using Treinamento.Net.Dominio.Entidade;
using Treinamento.Net.Dominio.Entidade.DTO;
using Treinamento.Net.Dominio.Entidade.Parametros;

namespace Treinamento.Net.Dominio.Interface.Repositorio
{
    public interface IPedidoRepositorio
    {
        public int incluir(Pedido pedido);
        bool alterarStatus(int id, int status);
        Pedido consultarPedido(int id);
        List<Pedido> listar();
    }
}
