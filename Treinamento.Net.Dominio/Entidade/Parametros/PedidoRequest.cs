﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade.Parametros
{
    public class PedidoRequest
    {
        public int numeroPedido { get; set; }
        public string cpfCliente { get; set; }
        public string cepCliente { get; set; }
        public List<PedidoItemRequest> itensProduto { get; set; }
    }
}
