﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade.Parametros
{
    public class PedidoItemRequest
    {
        public int quantidadeProduto { get; set; }
        public string eanProduto { get; set; }
        public decimal valorProduto { get; set; }
    }
}
