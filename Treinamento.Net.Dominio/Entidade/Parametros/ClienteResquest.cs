﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade.Parametros
{
    public class ClienteResquest
    {
        public int codigoCliente { get; set; }
        public string nomeCliente { get; set; }
        public string cpfCliente { get; set; }
    }
}
