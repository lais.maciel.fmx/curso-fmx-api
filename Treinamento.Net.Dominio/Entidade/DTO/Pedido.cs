﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade.DTO
{
    public class Pedido
    {
        public Pedido()
        {
            items = new List<PedidoItem>();
        }
        public int id { get; set; }
        public int codigoCliente { get; set; }
        public string cep { get; set; }
        public int statusPedido { get; set; }
        public List<PedidoItem> items { get; set; }
    }
}
