﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade.DTO
{
    public class PedidoItem
    {
        public int numeroPedido { get; set; }
        public int codigoProduto { get; set; }
        public decimal valorProduto { get; set; }
        public int quantidadeProduto { get; set; }
    }
}
