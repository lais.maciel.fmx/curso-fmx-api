﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade
{
    public class ProdutoResponse
    {
        public int codigoProduto { get; set; }
        public string codigoEan { get; set; }
        public string descricaoProduto { get; set; }
    }
}
