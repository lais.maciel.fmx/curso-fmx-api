﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treinamento.Net.Dominio.Entidade
{
    public class BaseResponse
    {
        public bool sucesso { get; set; }
        public string mensagem { get; set; }
        public object data { get; set; }
    }
}
